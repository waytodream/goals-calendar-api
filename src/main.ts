import { KoaJoiSwaggerServer } from './api/koa-joi-swagger/koa-joi-swagger.server';
import { Server } from './api/server.interface';

const port: string = process.env.PORT || '3300';
const swaggerHost: string = process.env.SWAGGER_HOST || `localhost:${port}`;

const server: Server = KoaJoiSwaggerServer.getInstance(swaggerHost);

server
  .listen(port)
  .then(() => {
    console.log(`Server running on port ${port}`);
  })
  .catch((err: string) => {
    console.error(`Error server running on port ${port}: `, err);
  });
