import { HttpStatusEnum } from '../../http-status.enum';
import { ResponseStatusEnum } from '../../response-status.enum';

export interface ResponseDataInterface<T> {
  code: HttpStatusEnum | ResponseStatusEnum;
  message?: string;
  data?: T;
}
