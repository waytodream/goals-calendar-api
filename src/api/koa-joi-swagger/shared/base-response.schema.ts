import { AnySchema, number, NumberSchema, object, string, StringSchema } from 'joi';
import { definition } from 'koa-joi-swagger-ts';
import { HttpStatusEnum } from '../../http-status.enum';
import { ResponseStatusEnum } from '../../response-status.enum';

/**
 * The schema not explicitly specified in koa-joi-swagger-ts as base response
 * It will be send at joi bad validation
 */
export class BaseKJSRouterResponseSchema {
  public code: NumberSchema = number()
    .required()
    .strict()
    .only(HttpStatusEnum.Success, HttpStatusEnum.BadRequest, HttpStatusEnum.InternalServerError)
    .example(HttpStatusEnum.Success)
    .description('Code of operation result');
  public message: StringSchema = string().description('message will be filled in some causes');
}

@definition('BaseResponseSchema', 'Base response entity with base fields')
export class BaseResponseSchema extends BaseKJSRouterResponseSchema {
  public data: AnySchema = object()
    .example({})
    .description('Date of operation result');

  constructor() {
    super();

    this.code = this.code.only(
      HttpStatusEnum.Success,
      HttpStatusEnum.BadRequest,
      HttpStatusEnum.InternalServerError,
      ResponseStatusEnum.Default
    );
  }
}
