import { string } from 'joi';
import {
  controller,
  description,
  ENUM_PARAM_IN,
  get,
  parameter,
  post,
  response,
  summary,
  tag,
} from 'koa-joi-swagger-ts';
import { RouterContext } from 'koa-router';

import { HttpStatusEnum } from '../../http-status.enum';
import { ResponseStatusEnum } from '../../response-status.enum';
import { BaseResponseSchema } from '../shared/base-response.schema';
import { ResponseDataInterface } from '../shared/response-data.interface';
import { userDtoSchema, UserResponseSchema, UsersResponseSchema } from './user-dto.schema';
import { UserDto } from './user.dto';

@controller('/users')
export abstract class UserController {
  @get('/')
  @response(HttpStatusEnum.Success, { $ref: UsersResponseSchema })
  @response(HttpStatusEnum.BadRequest, { $ref: BaseResponseSchema })
  @response(HttpStatusEnum.InternalServerError, { $ref: BaseResponseSchema })
  @tag('User')
  @description('Returns list of all users')
  @summary('Get all users')
  public async getAll(ctx: RouterContext): Promise<void> {
    try {
      const users: UserDto[] = await Promise.resolve([
        {
          id: 1,
          name: 'user1',
        },
      ]);

      ctx.status = HttpStatusEnum.Success;
      (ctx.body as ResponseDataInterface<UserDto[]>) = {
        code: ResponseStatusEnum.Default,
        data: users,
      };
    } catch (err) {
      ctx.status = HttpStatusEnum.InternalServerError;
      (ctx.body as ResponseDataInterface<UserDto[]>) = {
        code: ResponseStatusEnum.Default,
        message: `Failed: ${err}`,
      };
    }
  }

  @get('/{userId}')
  @parameter(
    'userId',
    string()
      .description('userId')
      .required(),
    ENUM_PARAM_IN.path
  )
  @response(HttpStatusEnum.Success, { $ref: UserResponseSchema })
  @response(HttpStatusEnum.BadRequest, { $ref: BaseResponseSchema })
  @response(HttpStatusEnum.InternalServerError, { $ref: BaseResponseSchema })
  @tag('User')
  @description('Returns object of user')
  @summary('Get one user')
  public async getId(ctx: RouterContext): Promise<void> {
    try {
      const user: UserDto = await new Promise(
        (resolve: (_: UserDto) => void, reject: (reason?: string) => void): void => {
          const users: UserDto[] = [
            {
              id: 1,
              name: 'user1',
            },
          ];

          const userFind: UserDto | undefined = users.find(
            (item: UserDto) => item.id === +ctx.params.userId
          );

          if (userFind) {
            resolve(userFind);
          } else {
            reject('user not found');
          }
        }
      );

      ctx.status = HttpStatusEnum.Success;
      (ctx.body as ResponseDataInterface<UserDto>) = {
        code: ResponseStatusEnum.Default,
        data: user,
      };
    } catch (err) {
      ctx.status = HttpStatusEnum.InternalServerError;
      (ctx.body as ResponseDataInterface<UserDto[]>) = {
        code: ResponseStatusEnum.Default,
        message: `Failed: ${err}`,
      };
    }
  }

  @post('/')
  @parameter('body', userDtoSchema.required(), ENUM_PARAM_IN.body)
  @response(HttpStatusEnum.Success, { $ref: UserResponseSchema })
  @response(HttpStatusEnum.BadRequest, { $ref: BaseResponseSchema })
  @response(HttpStatusEnum.InternalServerError, { $ref: BaseResponseSchema })
  @tag('User')
  @description('Returns created user')
  @summary('Create user')
  public async create(ctx: RouterContext): Promise<void> {
    ctx.status = HttpStatusEnum.Success;
    (ctx.body as ResponseDataInterface<UserDto>) = {
      code: ResponseStatusEnum.Default,
      data: ctx.request.body,
    };
    return;
  }
}
