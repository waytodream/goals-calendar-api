import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import { KJSRouter } from 'koa-joi-swagger-ts';
import logger from 'koa-logger';
import Router from 'koa-router';

import { Server } from '../server.interface';
import { BaseResponseSchema } from './shared/base-response.schema';
import { UserResponseSchema, UsersResponseSchema } from './user/user-dto.schema';
import { UserController } from './user/user.controller';

export class KoaJoiSwaggerServer implements Server {
  /**
   * singleton экземплюр
   */
  private static instance: KoaJoiSwaggerServer | undefined = undefined;

  private application: Koa;

  /**
   * Получение singleton экземпляра класса
   */
  public static getInstance(swaggerHost: string): KoaJoiSwaggerServer {
    if (KoaJoiSwaggerServer.instance === undefined) {
      KoaJoiSwaggerServer.instance = new KoaJoiSwaggerServer(swaggerHost);
    }
    return KoaJoiSwaggerServer.instance;
  }
  private constructor(swaggerHost: string) {
    this.application = new Koa();

    const router: Router = this.initRouters(swaggerHost);
    this.application.use(logger());
    this.application.use(bodyParser());
    this.application.use(router.routes());
    this.application.use(router.allowedMethods());
  }

  private initRouters(swaggerHost: string): Router {
    const router: KJSRouter = new KJSRouter({
      swagger: '2.0',
      info: {
        version: '1.0.0',
        title: 'simple-rest',
      },
      host: swaggerHost,
      basePath: '',
      schemes: ['http'],
      paths: {},
      definitions: {},
    });

    router.loadDefinition(BaseResponseSchema);
    router.loadDefinition(UserResponseSchema);
    router.loadDefinition(UsersResponseSchema);

    router.loadController(UserController);

    router.setSwaggerFile('swagger.json');
    router.loadSwaggerUI('/api/docs');

    return router.getRouter();
  }

  /**
   * Поднятие сервера
   * @param port Порт на котором будет поднял сервер
   */
  public async listen(port: number | string): Promise<void> {
    return new Promise<void>((resolve: () => void, reject: (reason?: string) => void): void => {
      this.application
        .listen(port, () => {
          resolve();
        })
        .on('error', (err: string) => {
          reject(err);
        });
    });
  }
}
