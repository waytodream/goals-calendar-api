import { BookModel } from './book.model';
import { BookRepositoryInterface } from './book.repository.interface';

export class BookService {
  constructor(private bookRepository: BookRepositoryInterface) {}

  public getAll(): BookModel[] {
    return this.bookRepository.getAll();
  }

  public getId(id: number): BookModel | undefined {
    return this.bookRepository.getId(id);
  }

  public create(book: BookModel): BookModel {
    return this.bookRepository.create(book);
  }

  public update(id: number, book: BookModel): BookModel | undefined {
    return this.bookRepository.update(id, book);
  }

  public delete(id: number): boolean {
    return this.bookRepository.delete(id);
  }
}
