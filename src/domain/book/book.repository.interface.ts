import { BookModel } from './book.model';

export interface BookRepositoryInterface {
  getAll(): BookModel[];

  getId(id: number): BookModel | undefined;

  create(book: BookModel): BookModel;

  update(id: number, book: BookModel): BookModel | undefined;

  delete(id: number): boolean;
}
