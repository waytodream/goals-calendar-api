import { BookModel } from '../../domain/book/book.model';
import { BookRepositoryInterface } from '../../domain/book/book.repository.interface';
import { BookEntity } from './book.entity';

const BOOKS: BookEntity[] = [
  {
    id: 1,
    name: 'hello',
  },
];

export class BookConstRepository implements BookRepositoryInterface {
  private id: number = 1;

  public getAll(): BookModel[] {
    return BOOKS.map(BookEntity.mapToModel);
  }

  public getId(id: number): BookModel | undefined {
    const book: BookEntity | undefined = BOOKS.find((item: BookEntity) => item.id === id);

    return book ? BookEntity.mapToModel(book) : book;
  }

  public create(book: BookModel): BookModel {
    const bookEntity: BookEntity = BookEntity.mapFromModel(book);

    bookEntity.id = ++this.id;
    BOOKS.push(bookEntity);

    return BookEntity.mapToModel(bookEntity);
  }

  public update(id: number, book: BookModel): BookModel | undefined {
    const bookIndex: number = BOOKS.findIndex((item: BookEntity) => item.id === id);

    if (bookIndex >= 0) {
      BOOKS.splice(bookIndex, 1, BookEntity.mapFromModel(book));

      return BOOKS[bookIndex];
    } else {
      return undefined;
    }
  }

  public delete(id: number): boolean {
    const bookIndex: number = BOOKS.findIndex((item: BookEntity) => item.id === id);

    if (bookIndex >= 0) {
      BOOKS.splice(bookIndex, 1);
      return true;
    } else {
      return false;
    }
  }
}
